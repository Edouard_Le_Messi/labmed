<?php

namespace App\Entity;

use App\Repository\InvitationLaboRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InvitationLaboRepository::class)
 */
class InvitationLabo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idLabo;

    /**
     * @ORM\Column(type="integer")
     */
    private $idMedecin;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdLabo(): ?int
    {
        return $this->idLabo;
    }

    public function setIdLabo(int $idLabo): self
    {
        $this->idLabo = $idLabo;

        return $this;
    }

    public function getIdMedecin(): ?int
    {
        return $this->idMedecin;
    }

    public function setIdMedecin(int $idMedecin): self
    {
        $this->idMedecin = $idMedecin;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
