<?php

namespace App\Entity;

use App\Repository\InvitationDRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InvitationDRepository::class)
 */
class InvitationD
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idD;

    /**
     * @ORM\Column(type="integer")
     */
    private $idMedecin;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdD(): ?int
    {
        return $this->idD;
    }

    public function setIdD(int $idD): self
    {
        $this->idD = $idD;

        return $this;
    }

    public function getIdMedecin(): ?int
    {
        return $this->idMedecin;
    }

    public function setIdMedecin(int $idMedecin): self
    {
        $this->idMedecin = $idMedecin;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
