<?php

namespace App\Entity;

use App\Repository\DelegueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DelegueRepository::class)
 *
 */
class Delegue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;




    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Labo::class, inversedBy="delegue", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $labo;


    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist"})
     *
     */
    protected $user;

    /**
     * Laboratoire constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLabo()
    {
        return $this->labo;
    }

    /**
     * @param mixed $labo
     * @return Delegue
     */
    public function setLabo($labo)
    {
        $this->labo = $labo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Delegue
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }




    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Delegue
     */
    public function setCreatedAt(\DateTime $createdAt): Delegue
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Delegue
     */
    public function setUpdatedAt(\DateTime $updatedAt): Delegue
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }




}
