<?php

namespace App\Entity;

use App\Repository\LaboRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass=LaboRepository::class)
 *
 */
class Labo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fax;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $matriculeFiscale;

    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist"})
     *
     */
    protected $user;



    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;



    /**
     * @ORM\OneToMany(targetEntity=Magazine::class, mappedBy="labo")
     */
    private $magazine;

    /**
     * Laboratoire constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');

        $this->magazine = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getMatriculeFiscale(): ?string
    {
        return $this->matriculeFiscale;
    }

    public function setMatriculeFiscale(string $matriculeFiscale): self
    {
        $this->matriculeFiscale = $matriculeFiscale;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Labo
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }



    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Labo
     */
    public function setCreatedAt(\DateTime $createdAt): Labo
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Labo
     */
    public function setUpdatedAt(\DateTime $updatedAt): Labo
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }



    public function __toString()
    {
        return $this->matriculeFiscale;
    }





    /**
     * @return Collection|Magazine[]
     */
    public function getMagazine(): Collection
    {
        return $this->magazine;
    }

    public function addMagazine(Magazine $magazine): self
    {
        if (!$this->magazine->contains($magazine)) {
            $this->magazine[] = $magazine;
            $magazine->setLabo($this);
        }

        return $this;
    }

    public function removeMagazine(Magazine $magazine): self
    {
        if ($this->magazine->contains($magazine)) {
            $this->magazine->removeElement($magazine);
            // set the owning side to null (unless already changed)
            if ($magazine->getLabo() === $this) {
                $magazine->setLabo(null);
            }
        }

        return $this;
    }



}
