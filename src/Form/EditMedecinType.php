<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;


class EditMedecinType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ...
            ->add('nom', TextType::class, array(
                'label' => false,
                'attr' => ['class' => 'form-control'] ))
            ->add('prenom', TextType::class,array(
                'label' => false,
                'attr' => ['class' => 'form-control'] ))
            ->add('adresse', TextType::class,array(
                'label' => false,
                'attr' => ['class' => 'form-control'] ))
            ->add('tel', TextType::class,array(
                'label' => false,
                'attr' => ['class' => 'form-control'] ))
            ->add('email', EmailType::class,array(
                'label' => false,
                'attr' => ['class' => 'form-control'] ))            // ...
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
