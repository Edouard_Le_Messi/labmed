<?php

namespace App\Repository;

use App\Entity\Invit;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Invit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Invit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Invit[]    findAll()
 * @method Invit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Invit::class);
    }


    public function countInvit($user)
    {
        return $this->createQueryBuilder('i')
            ->select('count(i.id)')
            ->andWhere('i.etat = :val')
            ->andWhere('i.recepteurId = :val2')
            ->setParameter('val', 'hold')
            ->setParameter('val2',$user )
            ->getQuery()
            ->getResult()
        ;
    }
    public function getInvit($user)
    {
        return $this->createQueryBuilder('i')

            ->andWhere('i.etat = :val')
            ->andWhere('i.recepteurId = :val2')
            ->setParameter('val', 'hold')
            ->setParameter('val2',$user )
            ->orderBy('i.createdAt', 'desc')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
            ;
    }

    public function getInvitBymed($user)
    {
        return $this->createQueryBuilder('i')

            ->andWhere('i.etat = :val')
            ->andWhere('i.recepteurId = :val2')

            ->setParameter('val', 'accepter')
            ->setParameter('val2',5 )

            ->orderBy('i.createdAt', 'desc')

            ->getQuery()
            ->getResult()
            ;
    }

    public function getlaboNotInvit($user)
    {
        return $this->createQueryBuilder('i')

            ->andWhere('i.etat = :val')
            ->andWhere('i.recepteurId = :val2')

            ->setParameter('val', 'accepter')
            ->setParameter('val2',$user )

            ->orderBy('i.createdAt', 'desc')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
            ;
    }



    /*
    public function findOneBySomeField($value): ?Invit
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
