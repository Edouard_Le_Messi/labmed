<?php

namespace App\Repository;

use App\Entity\InvitationLabo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InvitationLabo|null find($id, $lockMode = null, $lockVersion = null)
 * @method InvitationLabo|null findOneBy(array $criteria, array $orderBy = null)
 * @method InvitationLabo[]    findAll()
 * @method InvitationLabo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvitationLaboRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InvitationLabo::class);
    }

    // /**
    //  * @return InvitationLabo[] Returns an array of InvitationLabo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InvitationLabo
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
