<?php

namespace App\Controller;

use App\Entity\Delegue;
use App\Entity\User;
use App\Entity\InvitationD;
use App\Entity\Magazine;
use App\Entity\Medecin;
use App\Entity\Produit;
use App\Entity\Video;
use App\Form\DelegueType;
use App\Form\MagazineType;
use App\Form\ProduitType;
use App\Form\VideoType;
use App\Repository\DelegueRepository;
use App\Repository\InvitRepository;
use App\Repository\LaboRepository;
use App\Repository\MagazineRepository;
use App\Repository\UserRepository;
use App\Repository\ProduitRepository;
use App\Repository\VideoRepository;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/delegue" )
 */

class DelegueController extends AbstractController
{
    /**
     * @Route("/", name="delegue")
     */
    public function index(DelegueRepository $del)
    {
        return $this->render('delegue/index.html.twig', [
            'delegueInfo' => $del->findOneBy(['user'=> $this->getUser()]),

        ]);
    }
    /**
     * @Route("/produit", name="delegue_produit_index", methods={"GET"})
     */
    public function index_produit(ProduitRepository $produitRepository, DelegueRepository $delegueRepository): Response
    {

        return $this->render('delegue/produit_index.html.twig', [
            'produits' => $produitRepository->findBy(['users'=>$this->getUser()]),
            'delegueInfo' => $delegueRepository->findOneBy(['user'=> $this->getUser()]),
        ]);
    }

    /**
     * @Route("/produit/add", name="delegue_produit_add")
     */
    public function new_produit(Request $request, DelegueRepository $delegueRepository,UserRepository $repository):Response
    {
        $produit = new Produit();
        $form = $this->createForm(ProduitType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $produit->setUsers($this->getUser());


            $entityManager->persist($produit);
            $entityManager->flush();

            return $this->redirectToRoute('delegue_produit_index');
        }
        return $this->render("delegue/produit_new.html.twig" , [
            "form_title" => "ajouter un produit",
            "form_produit" => $form->createview(),
            'delegueInfo' => $delegueRepository->findOneBy(['user'=> $this->getUser()]),


    ]);

    }

    /**
     * @Route("/produit/madecins", name="delegue_medecin_list" , methods={"GET"})
     */
    public function listMedecin(DelegueRepository $delegueRepository)
    {
        $medecins = $this->getDoctrine()->getRepository(Medecin::class)->findAll();
         $delegueInfo =$delegueRepository->findOneBy(['user'=> $this->getUser()]);

         
          $invit =$this->getDoctrine()
        ->getRepository(InvitationD::class)
        ->findBy(['status' =>0,'idD' =>$delegueInfo->getId()]);
      

          $MedecinInvit = array();
         foreach ($invit as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(User::class)
            ->find($v->getIdMedecin());
             $labol =$this->getDoctrine()
            ->getRepository(Medecin::class)
            ->findOneBy(['user'=>$labo]);
             array_push($MedecinInvit, $labol);
         }
   
        return $this->render('delegue/medecin_list.html.twig', array(
            'medecins' => $medecins,
            'delegueInfo' => $delegueInfo,
            'invitation' => $MedecinInvit,

        ));
    }


        public function inviter($idMedecin, $idD)
    {
        $entityManager = $this->getDoctrine()->getManager();
        
         $notif = new InvitationD();
 
         $notif->setIdD($idD);
         $notif->setIdMedecin($idMedecin);
         $notif->setStatus(0);
           // ... persist the $product variable or any other work
        $entityManager->persist($notif);
        $entityManager->flush();
        return $this->redirectToRoute('delegue_medecin_list');
        
       
    }
    public function annuler($idMedecin, $idD)
    {
        $entityManager = $this->getDoctrine()->getManager();
        
         $notif =$this->getDoctrine()
        ->getRepository(InvitationD::class)
        ->findOneBy(['idD' => $idD, 'idMedecin'=>$idMedecin]);
        $entityManager->remove($notif);
        $entityManager->flush();
        return $this->redirectToRoute('delegue_medecin_list');
    }
    /**
     * @Route("/magazine", name="delegue_magazine_index", methods={"GET"})
     */
    public function index_magazine(MagazineRepository $magazineRepository, DelegueRepository $delegueRepository): Response
    {

        return $this->render('delegue/magazine_index.html.twig', [
            'magazines' => $magazineRepository->findBy(['users'=>$this->getUser()]),
            'delegueInfo' => $delegueRepository->findOneBy(['user'=> $this->getUser()]),

        ]);
    }

    /**
     * @Route("/magazine/add", name="delegue_magazine_add")
     */
    public function new_magazine(Request $request,DelegueRepository $delegueRepository, LaboRepository $laboRepository):Response
    {
        $magazine = new Magazine();
        $form = $this->createForm(MagazineType::class, $magazine);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $magazine->setUsers($this->getUser());

            $entityManager->persist($magazine);
            $entityManager->flush();

            return $this->redirectToRoute('delegue_magazine_index');
        }
        return $this->render("delegue/magazine_new.html.twig" , [
            "form_title" => "ajouter une magazine",
            "form_magazine" => $form->createview(),
            'delegueInfo' => $delegueRepository->findOneBy(['user'=> $this->getUser()]),

        ]);

    }
    /**
     * @Route("/video", name="delegue_video_index", methods={"GET"})
     */
    public function index_videos(VideoRepository $videoRepository, UserRepository $userRepository, InvitRepository $invitRepository , DelegueRepository $delegueRepository ): Response
    {

        dump($videoRepository->findBy(['users'=> $this->getUser()]));

        return $this->render('delegue/video_index.html.twig', [
            'videos' => $videoRepository->findBy(['users'=> $this->getUser()]),
            'delegueInfo' => $delegueRepository->findOneBy(['user'=> $this->getUser()]),


        ]);
    }
    /**
     * @Route("/video/add", name="delegue_video_add")
     */
    public function new_video(Request $request, DelegueRepository $delegueRepository, InvitRepository $invitRepository , UserRepository $userRepository):Response
    {
        $video = new Video();
        $form = $this->createForm(VideoType::class,$video);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $video->setUsers($this->getUser());

            $entityManager->persist($video);
            $entityManager->flush();

            return $this->redirectToRoute('delegue_video_index');
        }


        return $this->render('delegue/video_new.html.twig', [
            'video' => $video,
            'form' => $form->createView(),
            'delegueInfo' => $delegueRepository->findOneBy(['user'=> $this->getUser()]),

        ]);
    }

}
