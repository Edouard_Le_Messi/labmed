<?php

namespace App\Controller;

use App\Entity\Labo;
use App\Entity\User;
use App\Form\LaboType;
use App\Repository\LaboRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class SipController extends AbstractController
{
    /**
     * @Route("/sip/dashborad", name="sip")
     */
    public function index(UserRepository $userRepository)
    {

        return $this->render('sip/index.html.twig', [
            'user' => $this->getUser(),
        ]);
    }


    /**
     * @Route("/sip/dashborad/labo", name="sip_labo_index", methods={"GET"})
     */
    public function index_labo(LaboRepository $laboratoireRepository): Response
    {

        return $this->render('sip/labo_index.html.twig', [
            'laboratoires' => $laboratoireRepository->findAll(),
            'user' => $this->getUser()

        ]);
    }


    /**
     * @Route("/sip/dashborad/labo/add", name="sip_labo_add")
     */
    public function new_labo(Request $request, UserPasswordEncoderInterface $encoder):Response
    {
        $labo = new Labo();
        $form = $this->createForm(LaboType::class,$labo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $hash = $encoder->encodePassword($labo->getUser(),$labo->getUser()->getPassword());
            $labo->getUser()->setPassword($hash);
            $roles[] = 'ROLE_LABO';
            $labo->getUser()->setRoles($roles);


            $entityManager->persist($labo);
            $entityManager->flush();

            return $this->redirectToRoute('sip_labo_index');
        }


        return $this->render('sip/labo_new.html.twig', [
            'laboratoire' => $labo,
            'form' => $form->createView(),
            'user' => $this->getUser()
        ]);
    }


    /**
     * @Route("/sip/dashborad/labo/active/{id}", name="sip_labo_active")
     */
    public  function active_desactive_compte(Request $request, int $id, \Swift_Mailer $mailer): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find($id);
        $user->setIsActive(!$user->isActive());
        $entityManager->flush();

        if($user->isActive()){
            $message = (new \Swift_Message('Activation de compte'))
                ->setFrom('smartitpartner55@gmail.com')
                ->setTo($user->getEmail())
                ->setBody('Votre compte a été activé avec succès, vous pouvez accéder à votre espace maintenant.')
            ;
            $mailer->send($message);
        }




        return $this->redirectToRoute('sip_labo_index');
    }


}
