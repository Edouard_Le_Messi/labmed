<?php

namespace App\Controller;

use App\Entity\Invit;
use App\Repository\MagazineRepository;
use App\Repository\MedecinRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Criteria;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\VideoRepository;
use App\Repository\LaboRepository;
use App\Repository\InvitRepository;
use App\Repository\ProduitRepository;

use App\Entity\Notification;
use App\Entity\InvitationLabo;
use App\Entity\InvitationD;
use App\Entity\Medecin;
use App\Entity\Delegue;
use App\Entity\User;
use App\Entity\Labo;
use App\Form\Medecin1Type;
use App\Form\EditMedecinType;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use App\Form\VideoType;
use App\Entity\Produit;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Magazine;
use App\Entity\Video;

class MedecinController extends AbstractController
{
    /**
     * @Route("/medecin", name="medecin")
     */
    public function index(MedecinRepository $medecinRepository)
    {
        return $this->render('medecin/index.html.twig', [
            'medInfo' => $medecinRepository->findOneBy(['user' => $this->getUser()]),
        ]);
    }

//    /**
//     * @Route("/medecin/produit", name="medecin_produit_list")
//     * @return Response
//     */
//    public function Produitindex()
//    {
//        $produits = $this->getDoctrine()->getRepository(Produit::class)->findAll();
//        return $this->render('medecin/produit.html.twig', array('produits' => $produits));
//    }

    /**
     * @Route("/medecin/produit", name="medecin_produit_list", methods={"GET"})
     */
    public function index_produit(ProduitRepository $produitRepository, MedecinRepository $medecinRepository, InvitRepository $invitRepository): Response
    {
        $prods = [];
        foreach ($invitRepository->getInvitBymed($this->getUser()) as $u) {
            $prods = $produitRepository->findBy(['users' => $u->getUsers()]);
        }
      //  dump($prods);

        return $this->render('medecin/produit.html.twig', [
            'produits' => $prods,
            'medInfo' => $medecinRepository->findOneBy(['user' => $this->getUser()]),

//        return $this->render('medecin/produit.html.twig', [
//            'produits' => $produitRepository->findBy(
//                ['users'=> $laboRepository->findOneBy(['user'=> $this->getUser()])],
//                ['updatedAt' => 'DESC']
//
//            ),
//            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),
//            'getinvit' => $invitRepository->getInvit($this->getUser()),
//        ]);

        ]);
    }


    /**
     * @Route("/medecin/magazine", name="medecin_magazine_index",  methods={"GET"})
     * @return Response
     */

    public function index_magazine(MagazineRepository $magazineRepository, MedecinRepository $medecinRepository): Response
    {

        return $this->render('medecin/magazine_index.html.twig', [
            'magazines' => $magazineRepository->findAll(),
            'medInfo' => $medecinRepository->findOneBy(['user' => $this->getUser()]),


        ]);
    }


    /**
     * @Route("/video", name="medecin_video_index", methods={"GET"})
     */
    public function index_videos(VideoRepository $videoRepository, MedecinRepository $medecinRepository): Response
    {

        dump($videoRepository->findAll());

        return $this->render('medecin/video_index.html.twig', [
            'videos' => $videoRepository->findAll(),
            'medInfo' => $medecinRepository->findOneBy(['user' => $this->getUser()]),


        ]);

//        return $this->render('medecin/video_index.html.twig', [
//            'videos' => $videoRepository->findBy(
//                ['labo'=> $laboRepository->findOneBy(['user'=> $this->getUser()])],
//                ['updatedAt' => 'DESC']
//
//            ),
//            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),
//        ]);
    }


//    /**
//     * @Route("/video/add", name="medecin_video_add")
//     */
//    public function new_video(Request $request, LaboRepository $laboRepository):Response
//    {
//        $video = new Video();
//        $form = $this->createForm(VideoType::class,$video);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $entityManager = $this->getDoctrine()->getManager();
//
//            $video->setLabo($laboRepository->findOneBy(['user'=> $this->getUser()]));
//
//
//            $entityManager->persist($video);
//            $entityManager->flush();
//
//            return $this->redirectToRoute('medecin_video_index');
//        }
//
//
//        return $this->render('medecin/video_new.html.twig', [
//            'video' => $video,
//            'form' => $form->createView(),
//            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),
//        ]);
//    }


    /**
     * @Route("/medecin/amis", name="medecin_amis_index", methods={"GET"})
     */

    public function getLab(InvitRepository $invitRepository, MedecinRepository $medecinRepository)
    {
        return $this->render("medecin/labo_accepte.html.twig", [
            'getinvit' => $invitRepository->getInvitBymed($this->getUser()),
            'medInfo' => $medecinRepository->findOneBy(['user' => $this->getUser()]),
        ]);
    }


    /**
     * @Route("/medecin/labo", name="medecin_labo_index", methods={"GET"})
     */

    public function listLabo(InvitRepository $invitRepository, MedecinRepository $medecinRepository, LaboRepository $laboRepository, UserRepository $userRepository)
    {


$amis=[];
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find($this->getUser());

dump($user);


        return $this->render("medecin/labo_invit.html.twig", [
            'getinvit' => $invitRepository->getInvitBymed($this->getUser()),
            'medInfo' => $medecinRepository->findOneBy(['user' => $this->getUser()]),
            'notamis' => $amis
        ]);
    }


    /**
     * @Route("/medecin/send/{id}", name="medecin_invit_lab")
     */
    public  function  sendInvitLab(Request $request, int $id):Response
    {
        $invit = new Invit();
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find($id);

        $invit->setRecepteurId($user->getId());
        $invit->setUsers($this->getUser());
        $invit->setEtat("Hold");
        $entityManager->persist($invit);
        $entityManager->flush();


        return $this->redirectToRoute('medecin_labo_index');
    }
     
     /**
     * @Route("/medecin", name="medecin")
     */
    public function home(MedecinRepository $medecinRepository)
    {
        $notifN =$this->getDoctrine()
        ->getRepository(InvitationLabo::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);

         $NotificationDelegue =$this->getDoctrine()
        ->getRepository(InvitationD::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);
         $lab4 = array();
         $lab5 = array();
         foreach ($notifN as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Labo::class)
            ->find($v->getIdLabo());
             array_push($lab4, $labo);
         }
         foreach ($NotificationDelegue as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Delegue::class)
            ->find($v->getIdD());
             array_push($lab5, $labo);
         }
           $n =0;
           foreach ($notifN as $key) {
                       $n = $n +1;

           }
            $n1 =0;
           foreach ($NotificationDelegue as $key) {
                       $n1 = $n1 +1;

           }
           $N = $n + $n1;
        return $this->render('layout/index.html.twig',[
         'medecin'=> $this->getUser(),
         'labo4' =>$lab4,
         'labo5' =>$lab5,
         'n'=>$n,
         'n1'=>$n1,
         'N' => $N,

        ]);
     
    }

      /**
     * @Route("/medecin/profil/{id}" ,name="app_medecin_edit_medecin")
    */
    public function profil($id,Request $request, FileUploader $fileUploader)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $medecin =$this->getDoctrine()
        ->getRepository(User::class)
        ->find($id);



         $med = new User();
        $form = $this->createForm(Medecin1Type::class, $med);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $brochureFile */
            $brochureFile = $form->get('photoFile')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($brochureFile) {
                $brochureFileName = $fileUploader->upload($brochureFile);
                // instead of its contents
                $medecin->setPhotoName($brochureFileName);
            }

            // ... persist the $product variable or any other work
            $entityManager->persist($medecin);
            $entityManager->flush();
             return $this->redirectToRoute('app_medecin_edit_medecin', array('id' => $id));
        }
         $medecin1 =$this->getDoctrine()
        ->getRepository(Medecin::class)
        ->findOneBy(['user' => $medecin]);

         $form1 = $this->createForm(EditMedecinType::class, $medecin);
         $form1->handleRequest($request);
          if ($form1->isSubmitted() && $form1->isValid()) {
              $nom = $form->get('nom')->getData();
              $prenom = $form->get('prenom')->getData();
              $email = $form->get('email')->getData();
              $tel = $form->get('tel')->getData();
              $adresse = $form->get('adresse')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
           
                // instead of its contents
                $medecin->setNom($nom);
                $medecin->setPrenom($prenom);
                $medecin->setEmail($email);

                $medecin->setTel($tel);
                $medecin->setAdresse($adresse);
           
            // ... persist the $product variable or any other work
            $entityManager->persist($medecin);
            $entityManager->flush();
             return $this->redirectToRoute('app_medecin_edit_medecin', array('id' => $id));
        }
  
         $notifN =$this->getDoctrine()
        ->getRepository(InvitationLabo::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);

         $NotificationDelegue =$this->getDoctrine()
        ->getRepository(InvitationD::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);
         $lab4 = array();
         $lab5 = array();
         foreach ($notifN as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Labo::class)
            ->find($v->getIdLabo());
             array_push($lab4, $labo);
         }
         foreach ($NotificationDelegue as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Delegue::class)
            ->find($v->getIdD());
             array_push($lab5, $labo);
         }
           $n =0;
           foreach ($notifN as $key) {
                       $n = $n +1;

           }
            $n1 =0;
           foreach ($NotificationDelegue as $key) {
                       $n1 = $n1 +1;

           }
           $N = $n + $n1;
         return $this->render('medecin/profil.html.twig',[
         'form' => $form->createView(),
          'form1' => $form1->createView(),
         'medecin' => $medecin,
          'medecin1' => $medecin1,
          'labo4' =>$lab4,
         'labo5' =>$lab5,
         'n'=>$n,
         'n1'=>$n1,
         'N' => $N,


        ]);

    }
    public function produit($idMedecin)
    {
         $medecin =$this->getDoctrine()
        ->getRepository(User::class)
        ->find($idMedecin);

        $pro =$this->getDoctrine()
        ->getRepository(Produit::class)
        ->findAll();
       

          $notifsA =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'accepté','user' =>$idMedecin]);
         
//___________________________________________
         $lab1 = array();
         foreach ($notifsA as $v) {
             $labo =$this->getDoctrine()
            ->getRepository(Labo::class)
            ->find($v->getLabo());
             array_push($lab1, $labo);
         }
         $produits = array();

         foreach ($pro as $p) {
             foreach ($lab1 as $v1) {
                if ($v1->getUser()->getId() == $p->getUsers()->getId()) {
                    array_push($produits, $p);
                    break;
                }
             }
             
         }
         $notifN =$this->getDoctrine()
        ->getRepository(InvitationLabo::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);

         $NotificationDelegue =$this->getDoctrine()
        ->getRepository(InvitationD::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);
         $lab4 = array();
         $lab5 = array();
         foreach ($notifN as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Labo::class)
            ->find($v->getIdLabo());
             array_push($lab4, $labo);
         }
         foreach ($NotificationDelegue as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Delegue::class)
            ->find($v->getIdD());
             array_push($lab5, $labo);
         }
           $n =0;
           foreach ($notifN as $key) {
                       $n = $n +1;

           }
            $n1 =0;
           foreach ($NotificationDelegue as $key) {
                       $n1 = $n1 +1;

           }
           $N = $n + $n1;
        return $this->render('layout/produit.html.twig',[
         'produits' => $produits,
         'medecin' => $medecin,
         'labo4' =>$lab4,
         'labo5' =>$lab5,
         'n'=>$n,
         'n1'=>$n1,
         'N' => $N,
        ]);
    }

    /**
     * @Route("/medecin/detail/{id}")
     */
     public function detail($idMedecin,$id)
    {
        $produit =$this->getDoctrine()
        ->getRepository(Produit::class)
        ->find($id);
           $medecin =$this->getDoctrine()
        ->getRepository(User::class)
        ->find($idMedecin);
         $notifN =$this->getDoctrine()
        ->getRepository(InvitationLabo::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);

         $NotificationDelegue =$this->getDoctrine()
        ->getRepository(InvitationD::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);
         $lab4 = array();
         $lab5 = array();
         foreach ($notifN as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Labo::class)
            ->find($v->getIdLabo());
             array_push($lab4, $labo);
         }
         foreach ($NotificationDelegue as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Delegue::class)
            ->find($v->getIdD());
             array_push($lab5, $labo);
         }
           $n =0;
           foreach ($notifN as $key) {
                       $n = $n +1;

           }
            $n1 =0;
           foreach ($NotificationDelegue as $key) {
                       $n1 = $n1 +1;

           }
           $N = $n + $n1;
        return $this->render('layout/detail_Produit.html.twig',[
         'produit' => $produit,
         'medecin' => $medecin,
         'labo4' =>$lab4,
         'labo5' =>$lab5,
         'n'=>$n,
         'n1'=>$n1,
         'N' => $N,
        ]);
    }

     public function list_labo($idMedecin)
    {
        $medecin =$this->getDoctrine()
        ->getRepository(User::class)
        ->find($idMedecin);

          $labo =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->findAll();

          $notifsN =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'en entente','user' =>$idMedecin]);



         $notifsA =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'accepté','user' =>$idMedecin]);


         $notifsAll =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findAll();
         
//___________________________________________
          
//_____________________________________________

         $lab = array();
         $lab1 = array();
         foreach ($notifsN as $v) {
             $labol =$this->getDoctrine()
            ->getRepository(Labo::class)
            ->find($v->getLabo());
             array_push($lab, $labol);
         }
         $s=0;
         foreach ($labo as $v) {
             foreach ($notifsAll as $v1) {
               if ($v->getId() == $v1->getLabo()) {
                $s =1;
               }
             }
             if ($s==0) {
                array_push($lab1, $v);
             }
             
            $s=0;
         }
          $notifN =$this->getDoctrine()
        ->getRepository(InvitationLabo::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);

         $NotificationDelegue =$this->getDoctrine()
        ->getRepository(InvitationD::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);
         $lab4 = array();
         $lab5 = array();
         foreach ($notifN as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Labo::class)
            ->find($v->getIdLabo());
             array_push($lab4, $labo);
         }
         foreach ($NotificationDelegue as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Delegue::class)
            ->find($v->getIdD());
             array_push($lab5, $labo);
         }
           $n =0;
           foreach ($notifN as $key) {
                       $n = $n +1;

           }
            $n1 =0;
           foreach ($NotificationDelegue as $key) {
                       $n1 = $n1 +1;

           }
           $N = $n + $n1;
          return $this->render('medecin/List_labo.html.twig',[
          'labo' => $lab1,
          'medecin' => $medecin,
          'notifs' => $lab,
          'invitations' =>$notifsN,
          'labo4' =>$lab4,
         'labo5' =>$lab5,
         'n'=>$n,
         'n1'=>$n1,
         'N' => $N,
        ]);
    }
    public function notifier($idMedecin, $idLabo)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $medecin =$this->getDoctrine()
        ->getRepository(User::class)
        ->find($idMedecin);
         $labos=$this->getDoctrine()
        ->getRepository(Labo::class)
        ->find($idLabo);
         $notif = new Notification();

         $notif->setNom($medecin->getNom());
         $notif->setPrenom($medecin->getPrenom()); 
         $notif->setLabo($idLabo);
         $notif->setUser($idMedecin);
         $notif->setLname($labos->getUser()->getNom());
         $notif->setStatus("en entente");
           // ... persist the $product variable or any other work
        $entityManager->persist($notif);
        $entityManager->flush();
        return $this->redirectToRoute('app_medecin_list_labo', array('idMedecin' => $idMedecin));
        
       
    }
     public function annuler($idMedecin, $idLabo)
    {
        $entityManager = $this->getDoctrine()->getManager();
        
         $notif =$this->getDoctrine()
        ->getRepository(InvitationLabo::class)
        ->findOneBy(['idLabo' => $idLabo, 'idMedecin'=>$idMedecin]);
        $entityManager->remove($notif);
        $entityManager->flush();
        return $this->redirectToRoute('medecin');
    }

    public function valider($idMedecin, $idLabo){
       $entityManager = $this->getDoctrine()->getManager();
       $notif =$this->getDoctrine()
        ->getRepository(InvitationLabo::class)
        ->findOneBy(['idLabo' => $idLabo, 'idMedecin'=>$idMedecin]);
         $notif->setStatus(1);
         $entityManager->flush();
     return $this->redirectToRoute('medecin');
    }

    //Valider et rejeter invitation delegue

     public function annulerd($idMedecin, $idD)
    {
        $entityManager = $this->getDoctrine()->getManager();
        
         $notif =$this->getDoctrine()
        ->getRepository(InvitationD::class)
        ->findOneBy(['idD' => $idD, 'idMedecin'=>$idMedecin]);
        $entityManager->remove($notif);
        $entityManager->flush();
        return $this->redirectToRoute('medecin');
    }

    public function validerd($idMedecin, $idD){
       $entityManager = $this->getDoctrine()->getManager();
       $notif =$this->getDoctrine()
        ->getRepository(InvitationD::class)
        ->findOneBy(['idD' => $idD, 'idMedecin'=>$idMedecin]);
         $notif->setStatus(1);
         $entityManager->flush();
     return $this->redirectToRoute('medecin');
    }

    public function list_laboAccepte($idMedecin)
    {
          $notifs =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findAll();

         $medecin =$this->getDoctrine()
        ->getRepository(User::class)
        ->find($idMedecin);


          $labo =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->findAll();
         
//___________________________________________
       $notifsA =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'accepté','user' =>$idMedecin]);
         
//___________________________________________
           $lab1 = array();
         foreach ($notifsA as $v) {
             $labo =$this->getDoctrine()
            ->getRepository(Labo::class)
            ->find($v->getLabo());
             array_push($lab1, $labo);
         }
//_____________________________________________
$notifN =$this->getDoctrine()
        ->getRepository(InvitationLabo::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);

         $NotificationDelegue =$this->getDoctrine()
        ->getRepository(InvitationD::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);
         $lab4 = array();
         $lab5 = array();
         foreach ($notifN as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Labo::class)
            ->find($v->getIdLabo());
             array_push($lab4, $labo);
         }
         foreach ($NotificationDelegue as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Delegue::class)
            ->find($v->getIdD());
             array_push($lab5, $labo);
         }
           $n =0;
           foreach ($notifN as $key) {
                       $n = $n +1;

           }
            $n1 =0;
           foreach ($NotificationDelegue as $key) {
                       $n1 = $n1 +1;

           }
           $N = $n + $n1;

        return $this->render('medecin/List_labo_Accepte.html.twig',[
         'labo' => $lab1,
         'medecin' => $medecin,
         'labo4' =>$lab4,
         'labo5' =>$lab5,
         'n'=>$n,
         'n1'=>$n1,
         'N' => $N,
        ]);

    }

    public function produitParLabo($idMedecin, $idLabo)
    {
        $lab =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->find($idLabo);
         
         $user =$this->getDoctrine()
        ->getRepository(User::class)
        ->find($lab->getUser()->getId());

         $produits =$this->getDoctrine()
        ->getRepository(Produit::class)
        ->findBy(['users' => $user]);
        
         $medecin =$this->getDoctrine()
        ->getRepository(User::class)
        ->find($idMedecin);

        $notifN =$this->getDoctrine()
        ->getRepository(InvitationLabo::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);

         $NotificationDelegue =$this->getDoctrine()
        ->getRepository(InvitationD::class)
        ->findBy(['status' =>0,'idMedecin' =>$this->getUser()->getId()]);
         $lab4 = array();
         $lab5 = array();
         foreach ($notifN as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Labo::class)
            ->find($v->getIdLabo());
             array_push($lab4, $labo);
         }
         foreach ($NotificationDelegue as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(Delegue::class)
            ->find($v->getIdD());
             array_push($lab5, $labo);
         }
           $n =0;
           foreach ($notifN as $key) {
                       $n = $n +1;

           }
            $n1 =0;
           foreach ($NotificationDelegue as $key) {
                       $n1 = $n1 +1;

           }
           $N = $n + $n1;
       return $this->render('layout/list_labo_Produit.html.twig',[
         'produits' => $produits,
         'medecin' => $medecin,
         'labo4' =>$lab4,
         'labo5' =>$lab5,
         'n'=>$n,
         'n1'=>$n1,
         'N' => $N,
        ]); 
    }
}
