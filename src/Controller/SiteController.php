<?php

namespace App\Controller;

use App\Entity\Labo;
use App\Entity\Medecin;
use App\Form\LaboType;
use App\Form\MedecinType;
use App\Repository\LaboRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SiteController extends AbstractController
{
    /**
     * @Route("/", name="site")
     */
    public function index()
    {
        return $this->render('site/index.html.twig', [
            'controller_name' => 'SiteController',
        ]);
    }

    /**
     * @Route("/inscri_labo", name="inscri_labo")
     */
    public function inscri_labo(Request $request, UserPasswordEncoderInterface $encoder):Response
    {
        $labo = new Labo();
        $form = $this->createForm(LaboType::class,$labo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $hash = $encoder->encodePassword($labo->getUser(),$labo->getUser()->getPassword());
            $labo->getUser()->setPassword($hash);
            $roles[] = 'ROLE_LABO';
            $labo->getUser()->setRoles($roles);
            $labo->getUser()->setIsActive(false);
            $labo->getUser()->setUsername($labo->getUser()->getEmail());


            $entityManager->persist($labo);
            $entityManager->flush();

            return $this->redirectToRoute('security_login');
        }

        return $this->render('site/registration_labo.html.twig',[
            'laboratoire' => $labo,
            'form' => $form->createView(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/inscri_medecin", name="inscri_medecin")
     */
    public function inscri_medecin(Request $request, UserPasswordEncoderInterface $encoder, LaboRepository $laboRepository):Response
    {
        $medecin = new Medecin();
        $form = $this->createForm(MedecinType::class,$medecin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $hash = $encoder->encodePassword($medecin->getUser(),$medecin->getUser()->getPassword());
            $medecin->getUser()->setPassword($hash);
            $roles[] = 'ROLE_MEDECIN';
            $medecin->getUser()->setRoles($roles);
            $medecin->getUser()->setIsActive(false);




            $entityManager->persist($medecin);
            $entityManager->flush();

            return $this->redirectToRoute('security_login');
        }

        return $this->render('site/registration_medecin.html.twig', [
            'medecins' => $medecin,
            'form' => $form->createView(),
            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),
        ]);
    }


    /**
     * @Route("/creercompte", name="site_creer_compte")
     */
    public function  registration(Request $request, UserPasswordEncoderInterface $encoder):Response
    {
        $labo = new Labo();
        $medecin = new Medecin();

        $form = $this->createForm(LaboType::class,$labo);
        $form->handleRequest($request);


        $form_med = $this->createForm(MedecinType::class,$medecin);
        $form_med->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $hash = $encoder->encodePassword($labo->getUser(),$labo->getUser()->getPassword());
            $labo->getUser()->setPassword($hash);
            $roles[] = 'ROLE_LABO';
            $labo->getUser()->setRoles($roles);
            $labo->getUser()->setIsActive(false);
            $labo->getUser()->setUsername($labo->getUser()->getEmail());


            $entityManager->persist($labo);
            $entityManager->flush();

            return $this->redirectToRoute('security_login');
        }


        if ($form_med->isSubmitted() && $form_med->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $hash2 = $encoder->encodePassword($medecin->getUser(),$medecin->getUser()->getPassword());
            $medecin->getUser()->setPassword($hash2);
            $roles[] = 'ROLE_MEDECIN';
            $medecin->getUser()->setRoles($roles);
            $medecin->getUser()->setIsActive(false);
            $medecin->getUser()->setUsername($medecin->getUser()->getEmail());
            $entityManager->persist($medecin);
            $entityManager->flush();

            return $this->redirectToRoute('security_login');
        }
        return $this->render('site/registration.html.twig',[
            'laboratoire' => $labo,
            'form' => $form->createView(),
            'medecins' => $medecin,
            'form_med' => $form_med->createView(),
            'user' => $this->getUser()
        ]);
    }





}
