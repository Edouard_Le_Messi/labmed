<?php

namespace App\Controller;

use App\Entity\Delegue;
use App\Entity\Magazine;
use App\Entity\Medecin;
use App\Entity\Produit;
use App\Entity\Video;
use App\Entity\Invit;

use App\Entity\Notification;
use App\Entity\Labo;
use App\Entity\InvitationLabo;

use App\Entity\User;
use App\Form\DelegueType;
use App\Form\MagazineType;
use App\Form\MedecinType;
use App\Form\ProduitType;
use App\Form\VideoType;

use App\Repository\DelegueRepository;
use App\Repository\InvitRepository;
use App\Repository\LaboRepository;
use App\Repository\MagazineRepository;
use App\Repository\MedecinRepository;
use App\Repository\ProduitRepository;
use App\Repository\UserRepository;

use App\Repository\VideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/labo" )
 */
class LaboController extends AbstractController
{




    /**
     * @Route("/", name="labo")
     */
    public function index(LaboRepository $laboRepository, InvitRepository $invitRepository, UserRepository $userRepository):Response
    {
        dump($laboRepository->findOneBy(['user'=> $this->getUser()]));

     dump($invitRepository->getInvit($this->getUser()));
        $lab =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->findOneBy(['user' =>$this->getUser()]);

        $notifsN =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'en entente', 'Labo' =>$lab->getId()]);
         
//___________________________________________
            $n =0;
           $notifsNV = array();
           foreach ($notifsN as $key) {
                       $n = $n +1;

           }
//___________

        return $this->render('labo/index.html.twig', [
            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),

            'getinvit' => $notifsN,
            'n'=>$n,

        ]);
    }


    /**
     * @Route("/delegue", name="labo_delegue_index", methods={"GET"})
     */
    public function index_delegue(DelegueRepository $delegueRepository, LaboRepository $laboRepository, InvitRepository $invitRepository): Response
    {
        dump($delegueRepository->findBy(['labo'=> $laboRepository->findOneBy(['user'=> $this->getUser()])]));
            
      $lab =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->findOneBy(['user' =>$this->getUser()]);

        $notifsN =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'en entente', 'Labo' =>$lab->getId()]);
         
//___________________________________________
            $n =0;
           $notifsNV = array();
           foreach ($notifsN as $key) {
                       $n = $n +1;

           }
        return $this->render('labo/delegue_index.html.twig', [
            'delegues' => $delegueRepository->findBy(
                ['labo'=> $laboRepository->findOneBy(['user'=> $this->getUser()])],
                ['updatedAt' => 'DESC']

                ),
            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),
             'getinvit' => $notifsN,
            'n'=>$n,

        ]);
    }


    /**
     * @Route("/delegue/add", name="labo_delegue_add")
     */
    public function new_delegue(Request $request, UserPasswordEncoderInterface $encoder, LaboRepository $laboRepository, InvitRepository $invitRepository):Response
    {
        $delegue = new Delegue();
        $form = $this->createForm(DelegueType::class,$delegue);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $hash = $encoder->encodePassword($delegue->getUser(),$delegue->getUser()->getPassword());
            $delegue->getUser()->setPassword($hash);
            $roles[] = 'ROLE_DELEGUE';
            $delegue->getUser()->setRoles($roles);
            $delegue->setLabo($laboRepository->findOneBy(['user'=> $this->getUser()]));
            $delegue->getUser()->setUsername($delegue->getUser()->getEmail());
            $delegue->getUser()->setIsActive(true);

            $entityManager->persist($delegue);
            $entityManager->flush();

            return $this->redirectToRoute('labo_delegue_index');
        }
         $lab =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->findOneBy(['user' =>$this->getUser()]);

        $notifsN =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'en entente', 'Labo' =>$lab->getId()]);
         
//___________________________________________
            $n =0;
           $notifsNV = array();
           foreach ($notifsN as $key) {
                       $n = $n +1;

           }

        return $this->render('labo/delegue_new.html.twig', [
            'delegues' => $delegue,
            'form' => $form->createView(),
            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),
             'getinvit' => $notifsN,
            'n'=>$n,

        ]);
    }


    /**
     * @Route("/medecin", name="labo_medecin_index", methods={"GET"})
     */
    public function index_medecin(MedecinRepository $medecinRepository, LaboRepository $laboRepository,InvitRepository $invitRepository): Response
    {
         $list= $laboRepository->findOneBy(['user'=> $this->getUser()]);

          $notifN =$this->getDoctrine()
        ->getRepository(InvitationLabo::class)
        ->findBy(['status' =>0,'idLabo' =>$list->getId()]);

         $labs =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->findOneBy(['user' =>$this->getUser()]);

        $notifsN =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'en entente', 'Labo' =>$labs->getId()]);
         
//___________________________________________
            $n =0;
           $notifsNV = array();
           foreach ($notifsN as $key) {
                       $n = $n +1;

           }


          $lab = array();
         foreach ($notifN as $v) {
            $labo =$this->getDoctrine()
            ->getRepository(User::class)
            ->find($v->getIdMedecin());
             $labol =$this->getDoctrine()
            ->getRepository(Medecin::class)
            ->findOneBy(['user'=>$labo]);
             array_push($lab, $labol);
         }
        return $this->render('labo/medecin_index.html.twig', [
            'medecins' => $medecinRepository->findAll(),
            'laboInfo' =>$list,
             'getinvit' => $notifsN,
            'n'=>$n,
            'invitation' => $lab,

        ]);
    }


    /**
     * @Route("/medecin/add", name="labo_medecin_add")
     */
    public function new_medecin(Request $request, UserPasswordEncoderInterface $encoder, LaboRepository $laboRepository, InvitRepository $invitRepository):Response
    {
        $medecin = new Medecin();
        $form = $this->createForm(MedecinType::class,$medecin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $hash = $encoder->encodePassword($medecin->getUser(),$medecin->getUser()->getPassword());
            $medecin->getUser()->setPassword($hash);
            $roles[] = 'ROLE_MEDECIN';
            $medecin->getUser()->setRoles($roles);
            $medecin->getUser()->setUsername($medecin->getUser()->getEmail());
            $medecin->getUser()->setIsActive(true);


            $entityManager->persist($medecin);
            $entityManager->flush();

            return $this->redirectToRoute('labo_medecin_index');
        }
          $lab =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->findOneBy(['user' =>$this->getUser()]);

        $notifsN =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'en entente', 'Labo' =>$lab->getId()]);
         
//___________________________________________
            $n =0;
           $notifsNV = array();
           foreach ($notifsN as $key) {
                       $n = $n +1;

           }

        return $this->render('labo/medecin_new.html.twig', [
            'medecins' => $medecin,
            'form' => $form->createView(),
            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),
            'getinvit' => $notifsN,
            'n'=>$n,

        ]);
    }



    /**
     * @Route("/produit", name="labo_produit_index", methods={"GET"})
     */
    public function index_produit(ProduitRepository $produitRepository, LaboRepository $laboRepository, InvitRepository $invitRepository): Response
    {
      $lab =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->findOneBy(['user' =>$this->getUser()]);

        $notifsN =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'en entente', 'Labo' =>$lab->getId()]);
         
//___________________________________________
            $n =0;
           $notifsNV = array();
           foreach ($notifsN as $key) {
                       $n = $n +1;

           }

        return $this->render('labo/produit_index.html.twig', [
            'produits' => $produitRepository->findBy(
                ['users'=> $this->getUser()],
                ['updatedAt' => 'DESC']

            ),
            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),
            'getinvit' => $notifsN,
            'n'=>$n,

        ]);
    }


    /**
     * @Route("/produit/add", name="labo_produit_add")
     */
    public function new_produit(Request $request, LaboRepository $laboRepository, InvitRepository $invitRepository):Response
    {
        $produit = new Produit();
        $form = $this->createForm(ProduitType::class,$produit);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $produit->setUsers($this->getUser());


            $entityManager->persist($produit);
            $entityManager->flush();

            return $this->redirectToRoute('labo_produit_index');
        }
           $lab =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->findOneBy(['user' =>$this->getUser()]);

        $notifsN =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'en entente', 'Labo' =>$lab->getId()]);
         
//___________________________________________
            $n =0;
           $notifsNV = array();
           foreach ($notifsN as $key) {
                       $n = $n +1;

           }

        return $this->render('labo/produit_new.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),
             'getinvit' => $notifsN,
            'n'=>$n,

        ]);
    }
    /**
     * @Route("labo/active/{id}", name="accept")
     */
    public  function accepter(Request $request, int $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $invt = $entityManager->getRepository(Invit::class)->find($id);
        $invt->setEtat("accepter");
        $entityManager->flush();




        return $this->redirectToRoute('labo');
    }
    /**
     * @Route("labo/desactive/{id}", name="reject")
     */
    public  function rejecter(Request $request, int $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $invt = $entityManager->getRepository(Invit::class)->find($id);
        $invt->setEtat("rejecter");
        $entityManager->flush();




        return $this->redirectToRoute('labo');
    }

    /**
     * @Route("/video", name="labo_video_index", methods={"GET"})
     */
    public function index_videos(VideoRepository $videoRepository, UserRepository $userRepository, InvitRepository $invitRepository , LaboRepository $laboRepository ): Response
    {

        dump($videoRepository->findBy(['users'=> $this->getUser()]));
         
         $lab =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->findOneBy(['user' =>$this->getUser()]);

        $notifsN =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'en entente', 'Labo' =>$lab->getId()]);
         
//___________________________________________
            $n =0;
           $notifsNV = array();
           foreach ($notifsN as $key) {
                       $n = $n +1;

           }
        return $this->render('labo/video_index.html.twig', [
            'videos' => $videoRepository->findBy(['users'=> $this->getUser()]),
            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),
             'getinvit' => $notifsN,
            'n'=>$n,


        ]);
    }


    /**
     * @Route("/video/add", name="labo_video_add")
     */
    public function new_video(Request $request, LaboRepository $laboRepository, InvitRepository $invitRepository , UserRepository $userRepository):Response
    {
        $video = new Video();
        $form = $this->createForm(VideoType::class,$video);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $video->setUsers($this->getUser());

            $entityManager->persist($video);
            $entityManager->flush();

            return $this->redirectToRoute('labo_video_index');
        }

         $lab =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->findOneBy(['user' =>$this->getUser()]);

        $notifsN =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'en entente', 'Labo' =>$lab->getId()]);
//___________________________________________
            $n =0;
           $notifsNV = array();
           foreach ($notifsN as $key) {
                       $n = $n +1;

           }
        return $this->render('labo/video_new.html.twig', [
            'video' => $video,
            'form' => $form->createView(),
            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),
             'getinvit' => $notifsN,
            'n'=>$n,

        ]);
    }
    /**
     * @Route("/magazine", name="labo_magazine_index", methods={"GET"})
     */
    public function index_magazine(MagazineRepository $magazineRepository, LaboRepository $laboRepository, InvitRepository $invitRepository): Response
    {
          $lab =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->findOneBy(['user' =>$this->getUser()]);

        $notifsN =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'en entente', 'Labo' =>$lab->getId()]);
//___________________________________________
            $n =0;
           $notifsNV = array();
           foreach ($notifsN as $key) {
                       $n = $n +1;

           }
        return $this->render('labo/magazine_index.html.twig', [
            'magazines' => $magazineRepository->findBy(['users'=>$this->getUser()]),
            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),
            'getinvit' => $notifsN,
            'n'=>$n,

        ]);
    }

    /**
     * @Route("/magazine/add", name="labo_magazine_add")
     */
    public function new_magazine(Request $request, LaboRepository $laboRepository, InvitRepository $invitRepository):Response
    {
        $magazine = new Magazine();
        $form = $this->createForm(MagazineType::class, $magazine);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $magazine->setUsers($this->getUser());

            $entityManager->persist($magazine);
            $entityManager->flush();

            return $this->redirectToRoute('labo_magazine_index');
        }

         $lab =$this->getDoctrine()
        ->getRepository(Labo::class)
        ->findOneBy(['user' =>$this->getUser()]);

        $notifsN =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findBy(['status' =>'en entente', 'Labo' =>$lab->getId()]);
//___________________________________________
            $n =0;
           $notifsNV = array();
           foreach ($notifsN as $key) {
                       $n = $n +1;

           }
        return $this->render("labo/magazine_new.html.twig" , [
            "form_title" => "ajouter une magazine",
            "form_magazine" => $form->createview(),
            'laboInfo' => $laboRepository->findOneBy(['user'=> $this->getUser()]),
            'getinvit' => $notifsN,
            'n'=>$n,


        ]);

    }

      public function inviter($idMedecin, $idLabo)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $medecin =$this->getDoctrine()
        ->getRepository(User::class)
        ->find($idMedecin);
         $labos=$this->getDoctrine()
        ->getRepository(Labo::class)
        ->find($idLabo);
         $notif = new InvitationLabo();
 
         $notif->setIdLabo($idLabo);
         $notif->setIdMedecin($idMedecin);
         $notif->setStatus(0);
           // ... persist the $product variable or any other work
        $entityManager->persist($notif);
        $entityManager->flush();
        return $this->redirectToRoute('labo');
        
       
    }
   public function annuler($idMedecin, $idLabo)
    {
        $entityManager = $this->getDoctrine()->getManager();
        
         $notif =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->findOneBy(['Labo' => $idLabo]);
        $entityManager->remove($notif);
        $entityManager->flush();
        return $this->redirectToRoute('labo');
    }

    public function valider($idMedecin, $idNoti){
       $entityManager = $this->getDoctrine()->getManager();
       $notif =$this->getDoctrine()
        ->getRepository(Notification::class)
        ->find($idNoti);
        
        $notif->setStatus("accepté");
         $entityManager->flush();
     return $this->redirectToRoute('labo');
    }
    public function annulerM($idMedecin, $idLabo)
    {
        $entityManager = $this->getDoctrine()->getManager();
        
         $notif =$this->getDoctrine()
        ->getRepository(InvitationLabo::class)
        ->findOneBy(['idLabo' => $idLabo, 'idMedecin'=>$idMedecin]);
        $entityManager->remove($notif);
        $entityManager->flush();
        return $this->redirectToRoute('labo');
    }

}
