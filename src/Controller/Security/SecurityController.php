<?php

namespace App\Controller\Security;

use App\Entity\User;
use App\Form\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="security_registration")
     */
    public function registration(Request $request, EntityManagerInterface  $em,
                                 UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form  = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user,$user->getPassword());
            $user->setPassword($hash);

            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('security_login');
        }
        return $this->render('security/registration.html.twig',
            ['form' =>$form->createView()]);
    }

    /**
     * @Route("/connexion",name="security_login")
     */
    public function login(AuthenticationUtils  $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig',['lastUsername'=>$lastUsername,
            'error' => $error]);
    }

    /**
     * @Route("/deconnexion",name="security_logout")
     */
    public function logout()
    { }

    /**
     * Redirect users after login based on the granted ROLE
     * @Route("/login/redirect", name="login_redirect")
     */
    public function loginRedirectAction(Request $request)
    {
        if($this->isGranted('ROLE_ADMIN'))
        {
            return $this->redirectToRoute('sip_labo_index');
        }
        else if($this->isGranted('ROLE_LABO'))
        {
            return $this->redirectToRoute('labo');
        }
        else if($this->isGranted('ROLE_DELEGUE'))
        {
            return $this->redirectToRoute('delegue');
        }
        else if($this->isGranted('ROLE_MEDECIN'))
        {
            return $this->redirectToRoute('medecin');
        }
        else
        {
            return $this->redirectToRoute('security_login');
        }
    }




}
