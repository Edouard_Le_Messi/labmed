<?php


namespace App\Controller\Security;
use Symfony\Component\Security\Core\Exception\AccountStatusException;


class ExceptionCompteDesactive extends AccountStatusException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
        return 'Votre compte n\'est pas encore activé.';
    }
}
