<?php


namespace App\Controller\Security;
use App\Controller\Security\ExceptionCompteDesactive;
use App\Entity\User as AppUser;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;


class VerificateurUser implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }

        // L’utilisateur n’est pas activé par l’administrateur
        if (!$user->isActive()) {
            throw new ExceptionCompteDesactive();
        }
    }

    public function checkPostAuth(UserInterface $user)
    {

    }
}